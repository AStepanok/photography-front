export function vegas(selector, imgs, delay) {
    let parent = document.querySelector(selector);
    imgs.forEach((img, index) => {
        let div = document.createElement('div');
        div.id = 'vegas-'+index;
        div.className = 'slide';
        div.style.backgroundImage = "url('" + img + "')";
        if (index !== 0)
            div.style.opacity = '0';
        parent.appendChild(div);
    });

    let index = 0;
    let id = setInterval(function() {
        index++;
        if (index >= imgs.length) {
            index = 0;
            document.getElementById('vegas-'+(imgs.length-1)).style.opacity = 0;
        } else {
            document.getElementById('vegas-'+(index-1)).style.opacity = 0;
        }
        document.getElementById('vegas-'+index).style.opacity = 1;
    }, delay);
    return id;

}

export function stopVegas(id) {
    clearInterval(id);
}