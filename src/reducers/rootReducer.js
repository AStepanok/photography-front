import {combineReducers} from 'redux';
import textReducer from './textReducer';
import langReducer from './langReducer';
import vegasSlidesReducer from './vegasSlidesReducer';
import authReducer from './authReducer';
import pageReducer from "./pageReducer";
import categoriesReducer from "./categoriesReducer";
import albumsReducer from "./albumsReducer";
import priceBoxesReducer from "./priceBoxesReducer";
import reviewsReducer from "./reviewsReducer";

let rootReducer = combineReducers({
    texts: textReducer,
    lang: langReducer,
    vegasSlides: vegasSlidesReducer,
    loggedIn: authReducer,
    activePage: pageReducer,
    categories: categoriesReducer,
    albums: albumsReducer,
    priceBoxes: priceBoxesReducer,
    reviewPhotos: reviewsReducer
});

export default rootReducer
