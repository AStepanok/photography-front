import {actions} from "../actionCreators/constants";

const initialState = 'home' ;

export default function pageReducer(state = initialState, action) {
    switch (action.type){
        case actions.CHANGE_PAGE:
            return action.page;
        default:
            return state;
    }
}