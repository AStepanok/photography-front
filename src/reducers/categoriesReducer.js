import {actions} from "../actionCreators/constants";

const initialState = [];

export default function categoriesReducer(state = initialState, action) {
    let newCats;
    switch (action.type){
        case actions.EDIT_CATEGORY:
            newCats = state.map(c => {
                if (c._id === action.cat._id)
                    return action.cat;
                return c;
            });
            return [...newCats];

        case actions.GET_CATEGORIES:
            return [...action.cats];

        case actions.REMOVE_CATEGORY:
            newCats = state.filter(c => c._id !== action.category._id);
            return [...newCats];

        case actions.ADD_CATEGORY:
            return [...state, action.cat];

        default:
            return state;
    }
}
