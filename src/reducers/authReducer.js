import {actions} from "../actionCreators/constants";

const initialState = localStorage.getItem('token') ? true : false;

export default function authReducer(state = initialState, action) {
    switch (action.type){
        case actions.LOGIN:
            return true;
        case actions.LOGOUT:
            return false;
        default:
            return state;
    }
}