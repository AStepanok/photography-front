import {actions} from "../actionCreators/constants";

const initialState = [];

export default function textReducer(state = initialState, action) {
    switch (action.type){
        case actions.GET_TEXTS:
            return [...action.texts];
        case actions.EDIT_TEXT:
            let newTexts = state.map(t => {
                if (t._id === action.text._id)
                    return action.text;
                return t;
            });
            return [...newTexts];
        default:
            return state;
    }
}