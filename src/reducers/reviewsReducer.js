import {actions} from "../actionCreators/constants";

const initialState = [];

export default function reviewsReducer(state = initialState, action) {
    switch (action.type){
        case actions.GET_REVIEW_PHOTOS:
            return [...action.photos];
        case actions.EDIT_REVIEW_PHOTOS:
            return [...action.photos];
        default:
            return state;
    }
}
