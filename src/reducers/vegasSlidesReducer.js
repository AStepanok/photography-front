import {actions} from "../actionCreators/constants";

const initialState = [];

export default function vegasSlidesReducer(state = initialState, action) {
    switch (action.type){
        case actions.GET_SLIDES:
            return [...action.slides];
        case actions.EDIT_SLIDES:
            return [...action.slides];
        default:
            return state;
    }
}
