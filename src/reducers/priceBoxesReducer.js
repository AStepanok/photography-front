import {actions} from "../actionCreators/constants";

const initialState = [];

export default function priceBoxesReducer(state = initialState, action) {
    let newBoxes;
    switch (action.type){
        case actions.ADD_BOX:
            return [...state, action.box];

        case actions.REMOVE_BOX:
            newBoxes = state.filter(b => b._id !== action.boxId);
            return [...newBoxes];

        case actions.EDIT_BOX:
            newBoxes = state.map(b => {
                if (b._id === action.box._id)
                    return action.box;
                return b;
            });
            return [...newBoxes];

        case actions.GET_BOXES:
            return [...action.boxes];

        default:
            return state;
    }
}
