import {actions} from "../actionCreators/constants";

const initialState = localStorage.getItem('lang') !== null ? localStorage.getItem('lang') : 'rus';

export default function langReducer(state = initialState, action) {
    switch (action.type){
        case actions.LANG_CHANGE:
            localStorage.setItem('lang', state === 'rus' ? 'eng' : 'rus');
            return state === 'rus' ? 'eng' : 'rus';
        default:
            if (localStorage.getItem('lang') === null)
                localStorage.setItem('lang', 'rus');

            return state;
    }
}