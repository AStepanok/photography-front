import {actions} from "../actionCreators/constants";

const initialState = [];

export default function albumsReducer(state = initialState, action) {
    let newAlbums;
    switch (action.type){
        case actions.REMOVE_ALBUM:
            newAlbums = state.filter(a => a._id !== action.album._id);
            return [...newAlbums];

        case actions.EDIT_ALBUM:
            newAlbums = state.map(a => {
                if (a._id === action.album._id)
                    return action.album;
                return a;
            });
            return [...newAlbums];

        case actions.GET_ALBUMS:
            return [...action.albums];

        case actions.ADD_ALBUM:
            return [...state, action.album];

        default:
            return state;
    }
}
