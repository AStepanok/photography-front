import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';

// Components and actions
import Text from '../UI/Text';
import {getSlides} from "../../actionCreators/vegasSlideActions";

// Libraries and CSS and IMG
import {vegas, stopVegas} from "../../js/vegas-vanilla";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import logo from '../../img/logo.png';
import '../../css/Hero.css';
import '../../css/Admin.css';

class Hero extends Component {

    constructor(props) {
        super(props);
        this.state =  {
            sliderID: 0
        }
    }

    componentDidMount() {
        if (this.props.slides.length === 0)
            this.props.getSlides()
                .then(() => this.startVegas());
        else
            this.startVegas();
    }

    startVegas() {
        let id = vegas('.photoBack', this.props.slides.map(s => s.path), 5000);
        this.setState({sliderID: id});
    }

    componentWillUnmount() {
        stopVegas(this.state.sliderID);
    }

    render() {
        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
        const {texts, loggedIn} = this.props;
        return(
            <div className="hero">
                {loggedIn ?
                    <Link className='editAdminBtn' to='/foxy/slides/'>
                        <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white', position: 'absolute', right: '1%', top: '15%',
                        fontSize: '1.5rem', zIndex: '100'}}/></Link> : undefined}
                <div className='photoBack'/>
                <div className='infoContainer'>
                    <div className="info">
                        <img src={logo}/>
                        <h1>
                            <Text text={texts.find(t => t._id ==='5b521adc3bda3f04966b4522')}/>
                            {loggedIn ?
                            <Link className='editAdminBtn' to='/foxy/texts/5b521adc3bda3f04966b4522'>
                                <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white'}}/></Link> : undefined}
                                <br/>
                            <Text text={texts.find(t => t._id === '5b521aef3bda3f04966b4523')}/>
                            {loggedIn ?
                            <Link className='editAdminBtn' to='/foxy/texts/5b521aef3bda3f04966b4523'>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white'}}/></Link> : undefined}
                        </h1>
                        <p>
                            <Text text={texts.find(t => t._id === '5b521aa83bda3f04966b4521')}/>
                            {loggedIn ?
                            <Link className='editAdminBtn' to='/foxy/texts/5b521aa83bda3f04966b4521'>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white'}}/></Link> : undefined}
                        </p>
                        <Link to='/portfolio' className="btn" >
                            <Text text={texts.find(t => t._id === '5b521c303bda3f04966b4524')}/>
                        </Link>
                        {loggedIn ?
                        <Link className='editAdminBtn' to='/foxy/texts/5b521c303bda3f04966b4524'>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white'}}/></Link> : undefined}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(reduxState) {
    return {
        texts: reduxState.texts,
        slides: reduxState.vegasSlides,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getSlides})(Hero);