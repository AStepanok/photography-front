import React, {Component} from 'react';
import {connect} from "react-redux";
import {login, logout} from "../../actionCreators/authActions";
import '../../css/EditAndAddForm.css';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        }
    }

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.login(this.state);
    };

    handleLogout = (e) => {
        this.props.logout();
    };

    render() {
        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
        const {username, password} = this.state;
        const {loggedIn} = this.props;
        const btnStyle = {
            margin: 'auto',
            marginTop: '40px',
            padding: '10px 40px',
            display: 'block'
        };

        return(
            !loggedIn ?
            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            <h3>Привет!</h3>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="username">Логин</label>
                                    <input type="text"
                                           className="form-control"
                                           name='username'
                                           id='username'
                                           value={username}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Пароль</label>
                                    <input type="password"
                                           className="form-control"
                                           name='password'
                                           id='password'
                                           value={password}
                                           onChange={this.handleInputChange}
                                           required/>
                                </div>
                                <button type="submit" className="btn" style={btnStyle}>Вход</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> :
                <div className='create'>
                    <h3>Ты уже авторизирована!</h3>
                    <h3>Удачного пользования сайтом! :)</h3>
                    <button className="btn" style={btnStyle} onClick={this.handleLogout}>Выйти</button>
                </div>
        );
    }
}
function mapStateToProps(reduxState) {
    return {
        loggedIn : reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {login, logout})(Login);