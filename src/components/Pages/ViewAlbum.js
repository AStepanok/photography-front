import React, {Component} from 'react';
import {connect} from "react-redux";
import Popup from 'reactjs-popup';
import Masonry from 'react-masonry-component';
import InfiniteScroll from 'react-infinite-scroll-component';

import {getAlbums} from "../../actionCreators/albumActions";
import LazyImage from '../UI/LazyImage';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faSpinner} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/Popup.css'

class ViewAlbum extends Component {
    constructor(props) {
        super(props);

        this.state = {
            album : undefined,
            shownPhotos: [],
            hasMore: true
        }
    }

    componentDidMount() {
        if (this.props.albums.length === 0)
            this.props.getAlbums().then(() => {
                if (this.props.match.params && this.props.match.params.id) {
                    this.initPhotos()
                }
            });
        else if (this.props.match.params && this.props.match.params.id) {
            this.initPhotos()
        }
    }

    initPhotos() {
        const album = this.props.albums.find(a => a._id === this.props.match.params.id);
        if (album) {
            this.setState({album}, function () {
                let allPhotos = this.state.album.horizontalFirst ? [...this.state.album.photos.sort(this.horizontalFirst)] :
                    [...this.state.album.photos.sort(this.verticalFirst)];
                var totalLength = allPhotos.length;
                this.setState({
                    shownPhotos: [...allPhotos.slice(0, totalLength > 12 ? 12 : totalLength)],
                    hasMore: totalLength > 12
                })
            });
            document.title = this.props.lang === 'rus' ? album.nameRUS : album.nameENG;

        } else {
            this.props.history.push('/404');
        }
    }

    horizontalFirst(a,b) {
        if (a.orientation < b.orientation)
            return -1;
        if (a.orientation > b.orientation)
            return 1;
        return 0;
    }

    verticalFirst(a,b) {
        if (a.orientation > b.orientation)
            return -1;
        if (a.orientation < b.orientation)
            return 1;
        return 0;
    }

    handleLoadMorePhotos = () => {
        let allPhotos = this.state.album.horizontalFirst ? [...this.state.album.photos.sort(this.horizontalFirst)] :
            [...this.state.album.photos.sort(this.verticalFirst)];
        var currLength = this.state.shownPhotos.length;
        var totalLength = allPhotos.length;

        this.setState({
            shownPhotos: [...this.state.shownPhotos, ...allPhotos.slice(currLength,
                currLength+5 < totalLength ? currLength+5 : totalLength)],
            hasMore: totalLength > currLength + 5
        }, function () {

        })
    };

    render() {

        const {album} = this.props.album === undefined ? this.state : this.props;



        const {shownPhotos, hasMore} = this.state;

        const photos = album ? shownPhotos.map((photo, index) => (
            <div key={photo._id}>

                    {photo.orientation === 0 ? (
                        <Popup
                            trigger={<a><LazyImage srcPreload={photo.path_sm} srcLoaded={photo.path}/></a>}
                            modal
                            closeOnDocumentClick
                            contentStyle={{width: '90%',
                            maxWidth: '1200px'}}>
                            <img src={photo.path}/>
                        </Popup>
                    ) : (
                        <Popup
                        trigger={<a><LazyImage srcPreload={photo.path_sm} srcLoaded={photo.path}/></a>}
                        modal
                        closeOnDocumentClick
                        contentStyle={{width: '80%',
                            maxWidth: '700px'}}>
                        <img src={photo.path}/>
                        </Popup>
                    )}




            </div>
        )) : undefined;

        return (
            <div style={{marginTop: '80px'}} className='container-fluid albumPhotosContainer'>
                <div className='row'>
                    <div className='col-12'>

                        {photos && photos.length !== 0 ?
                            (
                                <InfiniteScroll
                                    dataLength={shownPhotos}
                                    next={this.handleLoadMorePhotos}
                                    hasMore={hasMore}
                                    scrollThreshold='0.9'
                                    loader={<FontAwesomeIcon icon={faSpinner} className='loader'/>}
                                    style={{overflow: 'hidden'}}>
                                    <Masonry className='photosContainer'
                                             updateOnEachImageLoad={true}
                                             style={{overflow: 'hidden'}}>
                                        {photos}
                                    </Masonry>
                                </InfiniteScroll>
                            ) : undefined}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(reduxState) {
    return {
        texts: reduxState.texts,
        albums: reduxState.albums,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getAlbums})(ViewAlbum);