import React, {Component} from 'react';
import {connect} from "react-redux";
import Slider from 'react-slick';
import {Link} from 'react-router-dom';

import {getBoxes} from "../../actionCreators/priceBoxActions";
import Text from '../UI/Text'
import PriceBox from '../UI/PriceBox';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPencilAlt, faSpinner, faPlus} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/Prices.css';


class Prices extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.priceBoxes.length === 0)
            this.props.getBoxes();
    }

    render() {
        document.title = this.props.lang === 'rus' ? 'Цены' : 'Prices';
        const settings = {
            dots: true,
            arrows: false,
            infinite: true,
            speed: 500,
            slidesToShow: this.props.priceBoxes.length > 3 ? 3 : this.props.priceBoxes.length,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: this.props.priceBoxes.length > 2 ? 2 : this.props.priceBoxes.length,
                        slidesToScroll: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        };

        const {loggedIn} = this.props;

        const boxes = this.props.priceBoxes.map(b => {
            return (
                <div key={b._id}>
                <PriceBox {...b}/>
                    {loggedIn ? <Link className='editAdminBtn' to={`/foxy/priceboxes/${b._id}`} >
                        <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333', display: 'block', textAlign: 'center', margin: 'auto'}}/></Link> : undefined}
                </div>
            )
        });

        return(
            <div className="price-boxes-container">
                <div className='container'>
                <div className="row">
                    <div className='col-12'>

                    <h1><Text text={this.props.texts.find(t => t._id ==='5b5db6786bb1226182a955ea')}/>{loggedIn ?
                        <Link className='editAdminBtn' to='/foxy/texts/5b5db6786bb1226182a955ea'>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h1>
                        {loggedIn ? <Link className='editAdminBtn' to={`/foxy/priceboxes/`} style={{display: 'block', margin: 'auto', fontSize: '1.5rem', textAlign:'center'}}>
                            <FontAwesomeIcon icon={faPlus} style={{color: '#333'}}/></Link> : undefined}
                    <Slider {...settings}>
                        {boxes}
                    </Slider>
                    </div>
                </div>
                </div>
            </div>



        );
    }
}

function mapStateToProps(reduxState) {
    return {
        texts : reduxState.texts,
        priceBoxes: reduxState.priceBoxes,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getBoxes})(Prices);