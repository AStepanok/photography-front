import React, {Component} from 'react';
import {connect} from "react-redux";
import Masonry from 'react-masonry-component';
import {Link} from 'react-router-dom';

import {getAlbums} from "../../actionCreators/albumActions";
import {getCategories} from "../../actionCreators/categoryActions";
import AlbumPreview from "../UI/AlbumPreview";
import InfiniteScroll from 'react-infinite-scroll-component';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPencilAlt, faSpinner, faPlus} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/Portfolio.css';



class Portfolio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeCategory: 'everything',
            shownAlbums: [],
            hasMore: true
        }
    }

    componentDidMount() {

        if (this.props.categories.length === 0)
            this.props.getCategories()
                .then(() => {
                    let activeCategory = this.props.categories.find(c => c.nameRUS == 'Портрет') ? this.props.categories.find(c => c.nameRUS == 'Портрет')._id : 'everything'
                    this.setState({
                        activeCategory
                    })
                });
        else {
            let activeCategory = this.props.categories.find(c => c.nameRUS == 'Портрет') ? this.props.categories.find(c => c.nameRUS == 'Портрет')._id : 'everything'
            this.setState({
                 activeCategory
            })
        }

        if (this.props.albums.length === 0)
            this.props.getAlbums()
                .then(() => this.initAlbums());
        else
            this.initAlbums();

    }

    initAlbums() {
        const allAlbums = this.props.loggedIn ? this.props.albums : this.props.albums.filter(a => a.showInPortfolio);
        const totalLength = allAlbums.length;
        this.setState({
            shownAlbums: [...allAlbums.slice(0, totalLength> 12 ? 12 : totalLength)],
            hasMore: totalLength > 12
        });

    }

    handleCategoryChanged = (id) => {
        this.setState({
            activeCategory: id
        })
    };

    handleLoadMoreAlbums = () => {
        const currLength = this.state.shownAlbums.length;
        const allAlbums = this.props.loggedIn ? this.props.albums : this.props.albums.filter(a => a.showInPortfolio);
        const totalLength = allAlbums.length;
        this.setState({
            shownAlbums: [...this.state.shownAlbums, ...allAlbums.slice(currLength,
                currLength+5 < totalLength ? currLength+5 : totalLength)],
            hasMore: totalLength > currLength + 5
        }, function () {
            console.log(this.state)
        })

    };

    sortByPriority(a,b) {
        if (a.priority < b.priority)
            return 1;
        if (a.priority > b.priority)
            return -1;
        return 0;
    }

    render() {
        const {shownAlbums, hasMore} = this.state;
        const {lang,loggedIn} = this.props;

        document.title = this.props.lang === 'rus' ? 'Портфолио' : 'Portfolio';

        let categories = this.props.categories.map((cat, index) => {
            return(
                <li key={cat._id}
                    className={cat._id == this.state.activeCategory ? 'active' : undefined}
                    onClick={this.handleCategoryChanged.bind(this,cat._id)}>{lang === 'rus' ? cat.nameRUS : cat.nameENG}
                    {loggedIn ? <Link className='editAdminBtn' to={`/foxy/categories/${cat._id}`}>
                        <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}
                    </li>
            )
        });

        categories = [(<li key='everything'
                           className={'everything' == this.state.activeCategory ? 'active' : undefined}
                           onClick={this.handleCategoryChanged.bind(this,'everything')}>{lang === 'rus' ? 'ВСЕ' : 'ALL'}</li>),
            ...categories];



        let filteredAlbums = [];
        if (shownAlbums.length>0) {
            filteredAlbums = shownAlbums.sort(this.sortByPriority).filter(a => a.category._id === this.state.activeCategory || this.state.activeCategory == 'everything');
            if (filteredAlbums.length<10 && hasMore)
                this.handleLoadMoreAlbums();
        }
        const albums = filteredAlbums.length>0 ? filteredAlbums.map((album, index) => {
            return (album.showInPortfolio && album.photos.length > 0) || loggedIn ? (
                <AlbumPreview album={album} key={album._id}/>
            ) : null;
        }): [];

        return(
            <div className='portfolioContainer'>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-12'>
                            <ul className='categories'>
                                {categories}
                                {loggedIn ? <li><Link className='editAdminBtn' to={`/foxy/categories/`}>
                                    <FontAwesomeIcon icon={faPlus} style={{color: '#333'}}/></Link></li> : undefined}
                            </ul>
                        </div>
                        <div className='col-12'>
                            {loggedIn ? <Link className='editAdminBtn' to={`/foxy/albums/`}>
                                <FontAwesomeIcon icon={faPlus} style={{color: '#333', display: 'block', margin: 'auto', fontSize: '1.5rem', marginBottom: '20px'}}/></Link> : undefined}
                            <InfiniteScroll
                                dataLength={filteredAlbums}
                                next={this.handleLoadMoreAlbums}
                                hasMore={hasMore}
                                scrollThreshold='0.9'
                                loader={<FontAwesomeIcon icon={faSpinner} className='loader'/>}>
                                <Masonry className='albumsContainer'>
                                    {albums}
                                </Masonry>
                            </InfiniteScroll>

                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

function mapStateToProps(reduxState) {
    return {
        lang: reduxState.lang,
        categories: reduxState.categories,
        albums: reduxState.albums,
        loggedIn: reduxState.loggedIn
    }
}

export default connect(mapStateToProps, {getCategories, getAlbums})(Portfolio);