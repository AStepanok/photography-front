import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import Slider from 'react-slick';

import Text from '../UI/Text'
import {getReviewPhotos} from "../../actionCreators/reviewActions";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/Reviews.css'

class Prices extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: ''
        }
    }

    componentDidMount() {
        if (this.props.reviews.length === 0)
            this.props.getReviewPhotos();


    }

    render() {
        document.title = this.props.lang === 'rus' ? 'Отзывы' : 'Reviews';

        const settings = {
            dots: true,
            infinite: true,
            arrows: false,
            speed: 500,
            slidesToShow: this.props.reviews.length > 3 ? 3 : this.props.reviews.length,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            cssEase: "linear",
            responsive: [
                {
                    breakpoint: 1000,
                    settings: {
                        slidesToShow: this.props.reviews.length > 2 ? 2 : this.props.reviews.length,
                        slidesToScroll: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
            ]
        };

        const {loggedIn} = this.props;

        const reviews = this.props.reviews.map(r => {
            return (
                <div key={r._id}>
                    <div style={{padding: '10px'}}>
                        <img src={r.path}/>
                    </div>

                </div>
            )
        });

        return(
            <div className="reviews-container">
                <div className='container'>
                    <div className="row">
                        <div className='col-12'>

                            <h1><Text text={this.props.texts.find(t => t._id ==='5b5db65a6bb1226182a955e9')}/>{loggedIn ?
                                <Link className='editAdminBtn' to='/foxy/texts/5b5db65a6bb1226182a955e9'>
                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h1>
                            {loggedIn ? <Link className='editAdminBtn' to={`/foxy/reviews/`} style={{display: 'block', margin: 'auto', fontSize: '1.5rem'}}>
                                <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333', display: 'block', textAlign: 'center', margin: 'auto'}}/></Link> : undefined}
                            <Slider {...settings}>
                                {reviews}
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>



        );
    }
}

function mapStateToProps(reduxState) {
    return {
        texts : reduxState.texts,
        reviews: reduxState.reviewPhotos,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getReviewPhotos})(Prices);