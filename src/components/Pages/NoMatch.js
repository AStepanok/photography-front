import React from 'react';
import '../../css/NoMatch.css'

const NoMatch = () => (
  <div className='noMatch'><h1>404 Not Found</h1></div>
);

export default NoMatch;