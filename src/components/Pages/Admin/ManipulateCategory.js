import React, {Component} from 'react';
import {connect} from "react-redux";
import {getCategories, addCategory, removeCategory, editCategory} from "../../../actionCreators/categoryActions";
import '../../../css/EditAndAddForm.css';

class CreateAlbum extends Component{
    constructor(props) {
        super(props);

        this.state = {
            ...this.props.category
        }
    }

    componentDidMount() {
        if (!this.props.loggedIn)
            this.props.history.push('/foxy');
        else {
            if (this.props.categories.length === 0)
                this.props.getCategories()
                    .then(() => {
                        if (this.props.match.params && this.props.match.params.id)
                            this.checkIfId()
                    });
            else if (this.props.match.params && this.props.match.params.id)
                this.checkIfId();
        }

        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';

    }

    checkIfId() {
        const category = this.props.categories.find(a => a._id === this.props.match.params.id);
        if (category)
            this.updateCategory(category);
        else
            this.props.history.push('/404');
    }

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state._id)
            this.props.editCategory(this.state)
                .then((cat) => {
                    this.props.history.push(`/portfolio`);
                });
        else
            this.props.addCategory(this.state)
                .then((cat) => {
                    this.props.history.push(`/portfolio`);
                });

    };

    handleRemoveCategory = (e) => {
        e.preventDefault();
        this.props.removeCategory(this.state)
            .then(() => {
                this.props.history.push('/portfolio');
            })
    };

    updateCategory(category) {
        this.setState({
            ...category
        });
    }

    render() {
        const {nameRUS, nameENG, _id} = this.state;


        return(

            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            {_id ?  <h3>Изменение категории</h3> : <h3>Добавление категории</h3>}
                                <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="nameRUS">Название категории на русском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='nameRUS'
                                           id='nameRUS'
                                           value={nameRUS}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="nameENG">Название категории на английском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='nameENG'
                                           id='nameENG'
                                           value={nameENG}
                                           onChange={this.handleInputChange}
                                           required/>
                                </div>
                                    <ul>

                                        <li><button type="submit" className="btn">{ _id ? 'Изменить категорию' : 'Создать категорию'}</button></li>
                                        {_id && (
                                            <li><button className="btn btn-danger" onClick={this.handleRemoveCategory}>Удалить категорию</button></li>
                                        )}
                                    </ul>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

function mapStateToProps(reduxState) {
    return {
        categories: reduxState.categories,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getCategories, addCategory, removeCategory, editCategory})(CreateAlbum);