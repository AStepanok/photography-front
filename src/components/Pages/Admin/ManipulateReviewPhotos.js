import React, {Component} from 'react';
import {connect} from "react-redux";
import Dropzone from 'react-dropzone';
import Masonry from 'react-masonry-component';
import {getReviewPhotos, uploadReviewPhotos, removeReviewPhotos} from "../../../actionCreators/reviewActions";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faTrash} from '@fortawesome/free-solid-svg-icons'
import '../../../css/EditAndAddForm.css';

class ManipulateReviewPhotos extends Component{
    constructor(props) {
        super(props);
        this.state = {
            reviews: []
        }

    }

    componentDidMount() {
        if (!this.props.loggedIn) {
            this.props.history.push('/foxy');
        } else {
            if (this.props.reviews.length === 0)
                this.props.getReviewPhotos().then((photos) => {
                    this.updatePhotos(photos)
                });
            else this.updatePhotos(this.props.reviews)
        }
        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
    }

    updatePhotos(reviews) {
        this.setState({
            reviews
        });
    }

    handleReviewUpload = (acceptedFiles) => {
        this.props.uploadReviewPhotos(acceptedFiles)
            .then((photos) => this.updatePhotos(photos));
    };

    handleReviewDelete = (id) => {
        this.props.removeReviewPhotos(id)
            .then((photos) => this.updatePhotos(photos));
    };


    render() {
        const reviews = this.state.reviews && this.state.reviews.length >0 ? this.state.reviews.map((photo, index) => (
            <div>
                <div className='photo'>
                    <img src={photo.path}/>
                    <ul>
                        <li><button onClick={this.handleReviewDelete.bind(this, photo._id)}><FontAwesomeIcon icon={faTrash}/></button></li>
                    </ul>
                </div>

            </div>
        )) : [];

        return(

            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            <h3>Управление отзывами</h3>
                            <div>
                                <div>
                                    <Dropzone onDrop={this.handleReviewUpload}
                                              className='dropzone'
                                              accept="image/jpeg, image/png, image/jpg"
                                    >
                                        <p>Перетащи фотографии в формате JPEG, JPG или PNG</p>
                                    </Dropzone>
                                    <h3>Все отзывы</h3>
                                    <small>Порядок такой же, как в сетке. Если нужен другой порядок, просто удали и загрузи фотки в нужном порядке </small>
                                    {reviews.length !== 0 ?
                                        (
                                            <Masonry className='photosContainer'>
                                                {reviews}
                                            </Masonry>
                                        ) : undefined}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

function mapStateToProps(reduxState) {
    return {
        loggedIn: reduxState.loggedIn,
        reviews: reduxState.reviewPhotos,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getReviewPhotos, uploadReviewPhotos, removeReviewPhotos})(ManipulateReviewPhotos);