import React, {Component} from 'react';
import {connect} from "react-redux";
import {editText} from "../../../actionCreators/textActions";
import '../../../css/EditAndAddForm.css';


class CreateAlbum extends Component{
    constructor(props) {
        super(props);

        this.state = {
            _id: '',
            rus: '',
            eng: ''
        }
    }

    componentDidUpdate() {
        if (!this.props.loggedIn)
            this.props.history.push('/foxy');
        else {
            if (this.props.texts.length === 0)
                this.props.getTexts()
                    .then(() => this.checkIfId());
            else if (this.props.match.params && this.props.match.params.id && this.state._id !== this.props.match.params.id)
                this.checkIfId()

        }
    }

    componentDidMount() {
        if (!this.props.loggedIn)
            this.props.history.push('/foxy');
        else {
            if (this.props.texts.length === 0)
                this.props.getTexts()
                    .then(() => this.checkIfId());
            else if (this.props.match.params && this.props.match.params.id && this.state._id !== this.props.match.params.id)
                this.checkIfId()

        }
        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
    }

    checkIfId() {
        const text = this.props.texts.find(t => t._id === this.props.match.params.id);
        if (text)
            this.setState({...text});
        else
            this.props.history.push('/404');
    }

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state._id)
            this.props.editText(this.state)
                .then(() => {
                    this.props.history.goBack();
                });

    };

    render() {
        const {rus, eng, _id} = this.state;
        const btnStyle = {
            margin: 'auto',
            marginTop: '40px',
            padding: '10px 40px',
            display: 'block'
        };

        return(
            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            <h3>Изменение текста</h3>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="rus">Текст на русском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='rus'
                                           id='rus'
                                           value={rus}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="eng">Текст на английском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='eng'
                                           id='eng'
                                           value={eng}
                                           onChange={this.handleInputChange}
                                           required/>
                                </div>
                                <button type="submit" className="btn" style={btnStyle}>Изменить текст</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

function mapStateToProps(reduxState) {
    return {
        texts: reduxState.texts,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {editText})(CreateAlbum);