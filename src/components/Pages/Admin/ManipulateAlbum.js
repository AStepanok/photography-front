import React, {Component} from 'react';
import {connect} from "react-redux";
import Masonry from 'react-masonry-component';
import Dropzone from 'react-dropzone';

// Actions
import {getAlbums, addAlbum, editAlbum, uploadPhotos, removeAlbum, removePhoto} from "../../../actionCreators/albumActions";
import {getCategories} from "../../../actionCreators/categoryActions";

// Libraries and CSS
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faImage, faTrash} from '@fortawesome/free-solid-svg-icons'
import '../../../css/EditAndAddForm.css';
import LazyImage from "../../UI/LazyImage";


class ManipulateAlbum extends Component{
    constructor(props) {
        super(props);
            this.state = {
                nameRUS: '',
                nameENG: '',
                category: '',
                secondLineRUS: '',
                secondLineENG: '',
                horizontalFirst: true,
                showInPortfolio: false,
                priority: 0
            }

    }

    componentDidMount() {
        if (!this.props.loggedIn) {
            this.props.history.push('/foxy');
        } else {
            if (this.props.categories.length === 0)
                this.props.getCategories().then(() => {
                    this.setState({category: this.props.categories[0]._id});
                });
            else {
                this.setState({category: this.props.categories[0]._id});
            }

            if (this.props.albums.length === 0)
                this.props.getAlbums().then(() => {
                    if (this.props.match.params && this.props.match.params.id)
                        this.checkIfId()
                });
            else if (this.props.match.params && this.props.match.params.id)
                this.checkIfId()
        }

        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';

    }

    checkIfId() {
        const album = this.props.albums.find(a => a._id === this.props.match.params.id);
        if (album)
            this.updateAlbum(album);
        else
            this.props.history.push('/404');
    }

    static horizontalFirst(a,b) {
        if (a.orientation < b.orientation)
            return -1;
        if (a.orientation > b.orientation)
            return 1;
        return 0;
    }

    static verticalFirst(a,b) {
        if (a.orientation > b.orientation)
            return -1;
        if (a.orientation < b.orientation)
            return 1;
        return 0;
    }

    updateAlbum(album) {
        if (album.horizontalFirst)
            album.photos.sort(ManipulateAlbum.horizontalFirst);
        else
            album.photos.sort(ManipulateAlbum.verticalFirst);

        this.setState({
            ...album
        }, function () {
            if (!album.previewPhoto)
                this.setState({
                    previewPhoto: undefined
                })
        });
    }

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleCheckboxChange = (e) => {
        this.setState({[e.target.name]: !this.state[e.target.name]})
    };

    handleSubmit = (e) => {
        if (e) e.preventDefault();
        if (this.state._id)
            this.props.editAlbum(this.state).then((album) => this.props.history.push(`/portfolio`));
        else {
            this.props.addAlbum(this.state).then((album) => this.props.history.push(`/foxy/albums/${album._id}`));
        }
    };

    handlePhotosUpload = (acceptedFiles) => {
        this.props.uploadPhotos(this.state._id, acceptedFiles)
            .then((album) => this.updateAlbum(album));
    };

    handlePhotoDelete = (id) => {
        this.props.removePhoto(this.state._id, id)
            .then((album) => this.updateAlbum(album));
    };

    handlePreviewPhotoUpdate = (id) => {
        this.setState({
            ...this.state,
            previewPhoto: id
        }, function () {
            this.props.editAlbum(this.state).then((album) => this.updateAlbum(album));
        });
    };

    handleRemoveAlbum = (e) => {
        e.preventDefault();
        this.props.removeAlbum(this.state)
            .then(() => {
                this.props.history.push('/portfolio');
            })
    };

    render() {
        const {_id, nameRUS, nameENG, secondLineENG, secondLineRUS, category, horizontalFirst,
        previewPhoto, showInPortfolio, priority} = this.state;


        const categories = this.props.categories.map((cat, index) => (
            <option value={cat._id} key={cat._id}>{cat.nameRUS}</option>
        ));

        const photos = this.state.photos ? this.state.photos.map((photo, index) => (
           <div>
               <div className='photo'>
                   <LazyImage srcLoaded={photo.path} srcPreload={photo.path_sm}/>
                   <ul>
                       <li><button onClick={this.handlePreviewPhotoUpdate.bind(this, photo._id)}><FontAwesomeIcon icon={faImage}/></button></li>
                       <li><button onClick={this.handlePhotoDelete.bind(this, photo._id)}><FontAwesomeIcon icon={faTrash}/></button></li>
                   </ul>
               </div>

           </div>
        )) : undefined;

        return(

            <div className='create'>
               <div className='container'>
                   <div className='row'>
                       <div className='col-12'>
                           <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                           {_id ? <h3>Изменение альбома</h3> : <h3>Добавление альбома</h3>}
                           <form onSubmit={this.handleSubmit}>
                               <div className="form-group">
                                   <label htmlFor="nameRUS">Название альбома на русском</label>
                                   <input type="text"
                                          className="form-control"
                                          name='nameRUS'
                                          id='nameRUS'
                                          value={nameRUS}
                                          onChange={this.handleInputChange}
                                          required/>

                               </div>
                               <div className="form-group">
                                   <label htmlFor="nameENG">Название альбома на английском</label>
                                   <input type="text"
                                          className="form-control"
                                          name='nameENG'
                                          id='nameENG'
                                          value={nameENG}
                                          onChange={this.handleInputChange}
                                          required/>
                               </div>
                               <div className="form-group">
                                   <label htmlFor="secondLineRUS">"Подназвание" альбома на русском</label>
                                   <input type="text"
                                          className="form-control"
                                          name='secondLineRUS'
                                          id='secondLineRUS'
                                          value={secondLineRUS}
                                          onChange={this.handleInputChange}
                                          required/>
                                   <small>Вторая строчка под названием альбома</small>
                               </div>
                               <div className="form-group">
                                   <label htmlFor="secondLineENG">"Подназвание" альбома на английском</label>
                                   <input type="text"
                                          className="form-control"
                                          name='secondLineENG'
                                          id='secondLineENG'
                                          value={secondLineENG}
                                          onChange={this.handleInputChange}
                                          required/>
                                   <small>Вторая строчка под названием альбома</small>
                               </div>
                               <div className="form-group">
                                   <label htmlFor="category">Категория альбома</label>
                                   <select className="form-control" name='category' id='category' value={category._id} onChange={this.handleInputChange}>
                                       {categories}
                                   </select>
                               </div>
                               <div className="form-group">
                                   <label htmlFor="priority">Приоритет альбома</label>
                                   <input type="number"
                                          className="form-control"
                                          name='priority'
                                          id='priority'
                                          value={priority}
                                          onChange={this.handleInputChange}
                                          required/>
                                   <small>Чем больше число, тем выше будет альбом в портфолио</small>
                               </div>
                               <div className="form-check">
                                   <input type='checkbox'
                                          className='form-check-input'
                                          name='horizontalFirst'
                                          id='horizontalFirst'
                                          checked={horizontalFirst}
                                          onChange={this.handleCheckboxChange}/>
                                   <label htmlFor="horizontalFirst" className='form-check-label'>Сначала горизонтальные</label>
                                    <div>
                                        <small>Для того чтобы минимизировать "лесенку" внизу фоток в альбоме, я придумал алгоритм сортировки.
                                            Загружай одинаковое кол-во горизонтальных и вертикальных фоток (по возможности), затем выбирай нужное значение и лесенки не будет</small>
                                    </div>

                               </div>

                               <div className="form-check">
                                   <input type='checkbox'
                                          className='form-check-input'
                                          name='showInPortfolio'
                                          id='showInPortfolio'
                                          checked={showInPortfolio}
                                          onChange={this.handleCheckboxChange}/>
                                   <label htmlFor="showInPortfolio" className='form-check-label'>Показывать на сайте</label>
                               </div>
                               <ul>

                                   <li><button type="submit" className="btn">{ _id ? 'Изменить альбом' : 'Создать альбом'}</button></li>
                               {_id && (
                                   <li><button className="btn btn-danger" onClick={this.handleRemoveAlbum}>Удалить альбом</button></li>
                               )}
                               </ul>
                           </form>
                           {_id ? (
                               <div>
                                   <div>
                                       <h3>Обложка альбома</h3>
                                       <img className='previewPhoto' src={previewPhoto ? previewPhoto.path : undefined}/>
                                   </div>
                                   <div>
                                       <Dropzone onDrop={this.handlePhotosUpload}
                                                 className='dropzone'
                                                 accept="image/jpeg, image/png, image/jpg"
                                       >
                                           <p>Перетащи фотографии в формате JPEG, JPG или PNG</p>
                                       </Dropzone>
                                       <h3>Фотографии в альбоме</h3>
                                       {photos.length !== 0 ?
                                           (
                                               <Masonry className='photosContainer'>
                                                   {photos}
                                               </Masonry>
                                           ) : undefined}

                                   </div>
                               </div>
                           ): undefined}
                       </div>
                   </div>
               </div>
            </div>


        )
    }
}

function mapStateToProps(reduxState) {
    return {
        categories: reduxState.categories,
        albums: reduxState.albums,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getCategories, getAlbums,
    addAlbum, editAlbum, uploadPhotos, removePhoto, removeAlbum})(ManipulateAlbum);