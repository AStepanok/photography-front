import React, {Component} from 'react';
import {connect} from "react-redux";
import Dropzone from 'react-dropzone';
import Masonry from 'react-masonry-component';
import {getSlides, uploadSlides, removeSlide} from "../../../actionCreators/vegasSlideActions";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faTrash} from '@fortawesome/free-solid-svg-icons'
import '../../../css/EditAndAddForm.css';


class ManipulateVegasSlides extends Component{
    constructor(props) {
        super(props);
        this.state = {
            slides: []
        }

    }

    componentDidMount() {
        if (!this.props.loggedIn) {
            this.props.history.push('/foxy');
        } else {
            if (this.props.slides.length === 0)
                this.props.getSlides().then((slides) => {
                    this.updateSlides(slides)
                });
            else this.updateSlides(this.props.slides)
        }
        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
    }

    updateSlides(slides) {
        this.setState({
            slides
        });
    }

    handleSlidesUpload = (acceptedFiles) => {

        this.props.uploadSlides(acceptedFiles)
            .then((slides) => this.updateSlides(slides));
    };

    handleSlideDelete = (id) => {
        this.props.removeSlide(id)
            .then((slides) => this.updateSlides(slides));
    };


    render() {
        const slides = this.state.slides && this.state.slides.length >0 ? this.state.slides.map((slide, index) => (
            <div>
                <div className='photo'>
                    <img src={slide.path} style={{filter: 'none'}}/>
                    <ul>
                        <li><button onClick={this.handleSlideDelete.bind(this, slide._id)}><FontAwesomeIcon icon={faTrash}/></button></li>
                    </ul>
                </div>

            </div>
        )) : [];

        return(

            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            <h3>Управление фотографиями в слайдшоу</h3>
                            <div>
                                <div>
                                    <Dropzone onDrop={this.handleSlidesUpload}
                                              className='dropzone'
                                              accept="image/jpeg, image/png, image/jpg"
                                    >
                                        <p>Перетащи фотографии в формате JPEG, JPG или PNG</p>
                                    </Dropzone>
                                    <h3>Фотографии в слайдшоу</h3>
                                    <small>Порядок такой же, как в сетке. Если нужен другой порядок, просто удали и загрузи фотки в нужном порядке </small>
                                    {slides.length !== 0 ?
                                        (
                                            <Masonry className='photosContainer'>
                                                {slides}
                                            </Masonry>
                                        ) : undefined}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

function mapStateToProps(reduxState) {
    return {
        loggedIn: reduxState.loggedIn,
        slides: reduxState.vegasSlides,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getSlides, removeSlide, uploadSlides})(ManipulateVegasSlides);