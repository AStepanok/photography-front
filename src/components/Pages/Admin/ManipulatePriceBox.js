import React, {Component} from 'react';
import {connect} from "react-redux";
import Dropzone from 'react-dropzone';
import {getBoxes, removeBox,uploadBoxBack, editBox,addBox} from "../../../actionCreators/priceBoxActions";
import '../../../css/EditAndAddForm.css';


class ManipulatePriceBox extends Component{
    constructor(props) {
        super(props);
        this.state = {
            nameRUS: '',
            nameENG: '',
            priceRUB: 0,
            priceUSD: 0,
            backgroundPhoto: '',
            features: [{
                nameRUS: '',
                nameENG: ''
            }]
        }

    }

    componentDidMount() {
        if (!this.props.loggedIn) {
            this.props.history.push('/foxy');
        } else {
            if (this.props.priceBoxes.length === 0)
                this.props.getBoxes()
                    .then(() => {
                        if (this.props.match.params && this.props.match.params.id)
                            this.checkIfId();
                    });
            else if (this.props.match.params && this.props.match.params.id)
                this.checkIfId();
        }

        document.title = this.props.lang === 'rus' ? 'Фотограф Дарья Паршина' : 'Daria Parshina Photography';
    }

    checkIfId() {
        const box = this.props.priceBoxes.find(b => b._id === this.props.match.params.id);
        if (box)
            this.updateBox(box);
        else
            this.props.history.push('/404');
    }

    updateBox(box) {
        this.setState({
            ...box
        });
    }

    handlePhotoUpload = (acceptedFiles) => {

        this.props.uploadBoxBack(this.state._id, acceptedFiles)
            .then((box) => this.updateBox(box));
    };

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleFeaturesInput = (e) => {
        let features = [...this.state.features];

        features[e.target.name.split('-')[1]][e.target.name.split('-')[2]] = e.target.value;

        this.setState({features});
    };

    handleNewFeatureLine = (e) => {
        e.preventDefault();
        if (this.state.features[this.state.features.length-1].nameRUS !== '' && this.state.features[this.state.features.length-1].nameENG !== '') {
            let features = [...this.state.features, {
                nameRUS: '',
                nameENG: ''
            }];
            this.setState({features});
        }
    };

    handleRemoveFeatureLine = (e) => {
        e.preventDefault();
        let features = [...this.state.features];
        features.pop();
        this.setState({features});

    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state._id)
            this.props.editBox(this.state)
                .then((box) => {
                    this.props.history.push(`/prices`);
                });
        else
            this.props.addBox(this.state)
                .then((box) => {
                    this.props.history.push(`/foxy/priceboxes/${box._id}`);
                });

    };

    handleRemoveBox = (e) => {
        e.preventDefault();
        this.props.removeBox(this.state._id)
            .then(() => {
                this.props.history.push('/prices');
            })
    };


    render() {
        const {nameRUS, nameENG, priceRUB, priceUSD, backgroundPhoto, _id} = this.state;

        const features = this.state.features.map((feature, index) => {
            return (
                <div>
                <div className="form-group">
                    <label htmlFor={`feature-${index}-nameRUS`}>Название фичи на русском</label>
                    <input type="text"
                           className="form-control"
                           name={`feature-${index}-nameRUS`}
                           id={`feature-${index}-nameRUS`}
                           value={feature.nameRUS}
                           onChange={this.handleFeaturesInput}
                           required/>

                </div>
                <div className="form-group">
                    <label htmlFor={`feature-${index}-nameENG`}>Название фичи на английском</label>
                    <input type="text"
                           className="form-control"
                           name={`feature-${index}-nameENG`}
                           id={`feature-${index}-nameENG`}
                           value={feature.nameENG}
                           onChange={this.handleFeaturesInput}
                           required/>

                </div>
                </div>
            )
        });

        return(

            <div className='create'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-12'>
                            <img className='foxy' src='http://i.siteapi.org/vDfCGUGBTLGN7Sn0X7DC5rLl258=/fit-in/1024x768/center/top/b2d930f3896b456.s.siteapi.org/img/95bc4bbe331ead05791b218fe208042419929d88.png' />
                            {_id ?  <h3>Изменение прайсбокса</h3> : <h3>Добавление прайсбокса</h3>}
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="nameRUS">Название услуги на русском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='nameRUS'
                                           id='nameRUS'
                                           value={nameRUS}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="nameENG">Название услуги на английском</label>
                                    <input type="text"
                                           className="form-control"
                                           name='nameENG'
                                           id='nameENG'
                                           value={nameENG}
                                           onChange={this.handleInputChange}
                                           required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="priceRUB">Цена услуги в рублях</label>
                                    <input type="number"
                                           className="form-control"
                                           name='priceRUB'
                                           id='priceRUB'
                                           value={priceRUB}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <div className="form-group">
                                    <label htmlFor="priceUSD">Цена услуги в долларах</label>
                                    <input type="number"
                                           className="form-control"
                                           name='priceUSD'
                                           id='priceUSD'
                                           value={priceUSD}
                                           onChange={this.handleInputChange}
                                           required/>

                                </div>
                                <h3>Фичи</h3>
                                {features}
                                <ul>

                                    <li><button className="btn" onClick={this.handleNewFeatureLine}>Добавить еще фичу</button></li>
                                    {features.length>1 && (
                                        <li><button className="btn btn-danger" onClick={this.handleRemoveFeatureLine}>Удалить последнюю фичу</button></li>
                                    )}
                                </ul>
                                <ul>

                                    <li><button type="submit" className="btn">{ _id ? 'Изменить прайсбокс' : 'Создать прайсбокс'}</button></li>
                                    {_id && (
                                        <li><button className="btn btn-danger" onClick={this.handleRemoveBox}>Удалить прайсбокс</button></li>
                                    )}
                                </ul>
                            </form>
                            {_id ? (
                                <div>
                                    <div>
                                        <h3>Фоновая картинка</h3>
                                        <img className='previewPhoto' src={backgroundPhoto !== '' ? backgroundPhoto : undefined}/>
                                    </div>
                                    <div>
                                        <Dropzone onDrop={this.handlePhotoUpload}
                                                  className='dropzone'
                                                  accept="image/jpeg, image/png, image/jpg"
                                        >
                                            <p>Перетащи фотографии в формате JPEG, JPG или PNG</p>
                                        </Dropzone>
                                    </div>
                                </div>
                            ): undefined}
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}

function mapStateToProps(reduxState) {
    return {
        loggedIn: reduxState.loggedIn,
        priceBoxes: reduxState.priceBoxes,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps, {getBoxes, removeBox, uploadBoxBack, editBox, addBox})(ManipulatePriceBox);