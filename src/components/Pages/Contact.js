import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import Popup from 'reactjs-popup';

// Components
import Text from '../UI/Text'
import {API_URL} from "../../actionCreators/constants";

// Libraries and CSS and IMG
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faEnvelope, faPencilAlt, faPhone} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/Contact.css';
import fb from '../../img/facebook.png';
import vk from '../../img/vk-com.png';
import inst from '../../img/instagram.png';

class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fio: '',
            phone: '',
            comments: '',
            showSuccessPopup: false,
            showErrorPopup: false
        }
    }

    handleInputChange = (e) => {
        this.setState({[e.target.name]: e.target.value});
    };

    handleSubmit = (e) => {
        e.preventDefault();

        fetch(`${API_URL}/email/`, {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({
                fio: this.state.fio,
                phone: this.state.phone,
                comments: this.state.comments
            })
            })
            .then(res => res.json())
            .then(res => {
                if (res.status === 200)
                    this.setState({
                        fio: '',
                        comments: '',
                        phone: '',
                        showSuccessPopup: true
                    });
                else
                    this.setState({
                        showErrorPopup: true
                    })
            })
            .catch(err => console.log(err));

    };

    render() {
        document.title = this.props.lang === 'rus' ? 'Контакты' : 'Contacts';
        const {loggedIn} = this.props;

        const {fio, comments, phone, showErrorPopup, showSuccessPopup} = this.state;

        return(
            <div className="contacts-container">
                <div className='container'>
                    <div className="row">
                        <div className='col-12 col-lg-6 mb-4 text-center text-lg-left'>
                            <h3><Text text={this.props.texts.find(t => t._id ==='5b5dd9b86bb1226182a955eb')}/>{loggedIn ?
                                <Link className='editAdminBtn' to='/foxy/texts/5b5dd9b86bb1226182a955eb'>
                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h3>
                            <ul>
                                <li><FontAwesomeIcon icon={faEnvelope}/><a href='mailto:parshinadaria14@gmail.com'>parshinadaria14@gmail.com</a></li>
                                <li><FontAwesomeIcon icon={faPhone}/><a href='tel:89852520850'>+7 (985) 252-08-50 <br/><small>Whatsapp, Telegram</small></a></li>
                                <li><a href='https://www.facebook.com/parshinadaria14'><img className='icon' src={fb}/></a>
                                    <a href='https://vk.com/daria.parshina'><img className='icon' src={vk}/></a>
                                    <a href='https://www.instagram.com/photo.dariaparshina/'><img className='icon' src={inst}/></a></li>
                            </ul>
                        </div>
                        <div className='col-12 col-lg-6 mb-4 text-center text-lg-left'>
                            <h3><Text text={this.props.texts.find(t => t._id ==='5b5dd9c36bb1226182a955ec')}/>{loggedIn ?
                                <Link className='editAdminBtn' to='/foxy/texts/5b5dd9c36bb1226182a955ec'>
                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h3>
                            <form method="post"  name="contactform" id="contactform" onSubmit={this.handleSubmit}>

                                    <div className="form-group">
                                        <label htmlFor="fio"><Text text={this.props.texts.find(t => t._id ==='5b5dd9d16bb1226182a955ed')}/>{loggedIn ?
                                            <Link className='editAdminBtn' to='/foxy/texts/5b5dd9d16bb1226182a955ed'>
                                                <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</label>
                                        <input type="text" className="form-control" id="fio" name='fio' value={fio} required onChange={this.handleInputChange}/>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="phone"><Text text={this.props.texts.find(t => t._id ==='5b5dd9e16bb1226182a955ee')}/>{loggedIn ?
                                            <Link className='editAdminBtn' to='/foxy/texts/5b5dd9e16bb1226182a955ee'>
                                                <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</label>
                                        <input type="number" className="form-control" id="phone" name='phone' value={phone} required onChange={this.handleInputChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="comments"><Text text={this.props.texts.find(t => t._id ==='5b5dd9f06bb1226182a955ef')}/>{loggedIn ?
                                            <Link className='editAdminBtn' to='/foxy/texts/5b5dd9f06bb1226182a955ef'>
                                                <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</label>
                                        <textarea name='comments' id='comments' className="form-control" value={comments} onChange={this.handleInputChange}/>
                                    </div>


                                    <button type='submit' className='btn'><Text text={this.props.texts.find(t => t._id ==='5b5dd9f86bb1226182a955f0')}/></button>{loggedIn ?
                                <Link className='editAdminBtn' to='/foxy/texts/5b5dd9f86bb1226182a955f0'>
                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}
                                <Popup
                                    modal
                                    closeOnDocumentClick
                                    open={showSuccessPopup}
                                    contentStyle={{width: '80%',
                                        maxWidth: '700px', borderRadius: '5px'}}>
                                    {close => (
                                        <div className='popup-body'>
                                            <a className="close" onClick={close}>
                                                &times;
                                            </a>
                                            <h1><Text text={this.props.texts.find(t => t._id ==='5b5ec10559a35a796a33174f')}/>{loggedIn ?
                                                <Link className='editAdminBtn' to='/foxy/texts/5b5ec10559a35a796a33174f'>
                                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h1>
                                        </div>
                                    )
                                    }
                                </Popup>
                                <Popup
                                    modal
                                    closeOnDocumentClick
                                    open={showErrorPopup}
                                    contentStyle={{width: '80%',
                                        maxWidth: '700px', borderRadius: '5px'}}>
                                    {close => (
                                        <div className='popup-body'>
                                            <a className="close" onClick={close}>
                                                &times;
                                            </a>
                                            <h1><Text text={this.props.texts.find(t => t._id ==='5b5ec12f59a35a796a331750')}/>{loggedIn ?
                                                <Link className='editAdminBtn' to='/foxy/texts/5b5ec12f59a35a796a331750'>
                                                    <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}</h1>
                                        </div>
                                    )
                                    }
                                </Popup>
                            </form>

                        </div>

                    </div>
                </div>
            </div>



        );
    }
}

function mapStateToProps(reduxState) {
    return {
        texts : reduxState.texts,
        loggedIn: reduxState.loggedIn,
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps)(Contact);