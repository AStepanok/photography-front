import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faPencilAlt} from "@fortawesome/free-solid-svg-icons/index";
import '../../css/AlbumPreview.css';
import LazyImage from "./LazyImage";

class AlbumPreview extends Component {
    constructor(props) {
        super(props);

        this.state ={
            style:{
                transform: 'scale(0)',
            }
        }
    }


    render() {
        const {album, lang, loggedIn} = this.props;

        return (
            <div style={this.state.style}>
                <Link to={`/portfolio/${album._id}`}>
                <div className='photo'>
                    {typeof album.previewPhoto === 'object' && album.previewPhoto !== null ?
                        <LazyImage srcPreload={album.previewPhoto.path_sm} srcLoaded={album.previewPhoto.path}/> :
                        <LazyImage srcPreload={'https://parshina.rocketwebsites.ru/nophoto.png'} srcLoaded={'https://parshina.rocketwebsites.ru/nophoto.png'}/>
                    }
                    <ul>
                        <li><h5>{lang === 'rus' ? album.nameRUS : album.nameENG}</h5></li>
                        <li><h6>{lang === 'rus' ? album.secondLineRUS : album.secondLineENG}</h6></li>
                        {loggedIn ? <li><Link className='editAdminBtn' to={`/foxy/albums/${album._id}`}>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: 'white'}}/></Link></li> : undefined}
                    </ul>
                </div>
                </Link>
            </div>
        );
    }
}

function mapStateToProps(reduxState) {
    return {
        lang: reduxState.lang,
        loggedIn: reduxState.loggedIn
    }
}

export default connect(mapStateToProps)(AlbumPreview);