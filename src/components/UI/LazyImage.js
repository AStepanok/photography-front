import React, { Component } from 'react';
import '../../css/LazyImage.css';
import {faSpinner} from "@fortawesome/free-solid-svg-icons/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class LazyImage extends Component {

    constructor(props) {
        super(props);
        this.lazyImageHd = null;
    }

    componentDidMount() {

        const hdLoaderImg = new Image();

        hdLoaderImg.src = this.props.srcLoaded;

        hdLoaderImg.onload = () => {
            this.lazyImageHd.setAttribute(
                'style',
                `background-image: url('${this.props.srcLoaded}')`
            );
            this.lazyImageHd.classList.add('iron-image-fade-in');
        }



    };

    render() {
        return (
            <div className="photo">

                <div
                    className="iron-image-loaded"
                    ref={imageLoadedElem => this.lazyImageHd = imageLoadedElem}>
                </div>
                <img src={this.props.srcPreload}/>
                <FontAwesomeIcon icon={faSpinner} className='loader'/>

            </div>
        )
    }

}

export default LazyImage;