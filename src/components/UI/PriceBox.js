import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';

import Text from '../UI/Text'

import {faPencilAlt} from "@fortawesome/free-solid-svg-icons/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import '../../css/Prices.css';


class PriceBox extends Component {

    render() {
        const {nameRUS, nameENG, priceRUB, priceUSD, backgroundPhoto, lang, loggedIn} = this.props;

        const features = this.props.features.map((f, index) => {
            return (
                <li key={index}>{lang === 'rus' ? f.nameRUS : f.nameENG}</li>
            )
        });


        return(
            <div className='price-box-container'>
                <div className="price-box ">
                    <div className="pr-box price-heading bg-image" style={{backgroundImage: backgroundPhoto !== '' ? `url(${backgroundPhoto})` :
                            'url(http://localhost:8083/nophoto.png)'}}>
                        <div className="price-heading-inner">
                            <h3 className="price-title">{lang === 'rus' ? nameRUS : nameENG}</h3>
                        </div>
                    </div>
                    <div className="pr-box price-box-price">
                        <div className="price"><span className="price-currency">{lang === 'rus' ? '₽' : '$'}</span>{lang === 'rus' ? priceRUB :
                            priceUSD}</div>

                    </div>
                    <div className="pr-box price-features">
                        <ul className="list-unstyled">
                            {features}
                        </ul>
                    </div>
                    <div className='pr-box'>
                        <Link to='/contacts' className='btn'><Text text={this.props.texts.find(t => t._id ==='5b5dd9fd6bb1226182a955f1')}/></Link>{loggedIn ?
                        <Link className='editAdminBtn' to='/foxy/texts/5b5dd9fd6bb1226182a955f1'>
                            <FontAwesomeIcon icon={faPencilAlt} style={{color: '#333'}}/></Link> : undefined}
                    </div>

                </div>
            </div>
        );
    }
}

function mapStateToProps(reduxState) {
    return {
        lang : reduxState.lang,
        loggedIn: reduxState.loggedIn,
        texts: reduxState.texts
    }
}

export default connect(mapStateToProps)(PriceBox);