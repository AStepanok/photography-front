import React, {Component} from 'react';
import {connect} from 'react-redux';

class Text extends Component {
    render() {
        const {text, lang} = this.props;
        return(
            <span>{text ? text[lang] : ''}</span>
        )
    }
}

function mapStateToProps(reduxState) {
    return {
        lang: reduxState.lang
    }
}

export default connect(mapStateToProps)(Text);