import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';
import Popup from 'reactjs-popup'

import Text from './Text';
import {changePage} from "../../actionCreators/pageActions";
import {getTexts} from '../../actionCreators/textActions';
import {changeLang} from "../../actionCreators/langActions";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars, faPencilAlt } from '@fortawesome/free-solid-svg-icons'
import '../../css/Navbar.css';
import ruIMG from '../../img/TextRU.png';
import enIMG from '../../img/TextEN.png';

class Navbar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editableText: undefined
        }
    }

    handleCollapse = () => {
        var content = document.getElementById('target');
        if (content.style.maxHeight){
            content.style.maxHeight = null;
        } else {
            content.style.maxHeight = content.scrollHeight + "px";
        }
    };

    handleChangeLang = () => {
        this.props.changeLang();
    };

    handleChangePage = (page) => {
        this.props.changePage(page);
    };
    
    componentDidMount() {
        if (this.props.texts.length === 0)
            this.props.getTexts();

        let url = window.location.href.split('/');
        if (url.length >= 4)
            this.props.changePage(url[3]);
    }

    render() {
        const {texts, lang, activePage, loggedIn} = this.props;
        const {editableText} = this.state;

        return(
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <Link to='/'
                              className="navbar-brand float-left float-lg-none"
                              onClick={this.handleChangePage.bind(this,'hero')}>Daria Parshina</Link>
                        <button className="toggler d-inline-block d-lg-none float-right float-lg-none" onClick={this.handleCollapse}>
                            <FontAwesomeIcon icon={faBars}/>
                        </button>
                    </div>
                    <div id='target' className='collapsable'>
                        <ul className="nav navbar-nav d-flex flex-column flex-lg-row align-items-lg-center">
                            <li>
                                <Link to='/portfolio' onClick={this.handleChangePage.bind(this,'portfolio')}
                                      className={activePage == 'portfolio' ? 'active' : undefined}><Text text={texts.find(t => t._id == '5b51be5b3bda3f04966b451e')}/>
                                </Link>
                                {loggedIn && texts.length > 0?
                                    <Link to={`/foxy/texts/5b51be5b3bda3f04966b451e`} className='editAdminBtn'>
                                        <FontAwesomeIcon icon={faPencilAlt}/>
                                    </Link> : undefined
                                }
                            </li>
                            <li>
                                <Link to='/prices' onClick={this.handleChangePage.bind(this,'prices')}
                                      className={activePage == 'prices' ? 'active' : undefined}><Text text={texts.find(t => t._id == '5b5227e93bda3f04966b4525')}/>
                                </Link>
                                {loggedIn && texts.length > 0?
                                    <Link to={`/foxy/texts/5b5227e93bda3f04966b4525`} className='editAdminBtn'>
                                        <FontAwesomeIcon icon={faPencilAlt}/>
                                    </Link> : undefined
                                }
                            </li>
                            <li>
                                <Link to='/reviews' onClick={this.handleChangePage.bind(this,'reviews')}
                                      className={activePage == 'reviews' ? 'active' : undefined}><Text text={texts.find(t => t._id == '5b5227f13bda3f04966b4526')}/>
                                </Link>
                                {loggedIn && texts.length > 0?
                                    <Link to={`/foxy/texts/5b5227f13bda3f04966b4526`} className='editAdminBtn'>
                                        <FontAwesomeIcon icon={faPencilAlt}/>
                                    </Link> : undefined
                                }
                            </li>
                            <li>
                                <Link to='/contacts' onClick={this.handleChangePage.bind(this,'contacts')}
                                      className={activePage == 'contacts' ? 'active' : undefined}><Text text={texts.find(t => t._id == '5b51e22a3bda3f04966b451f')}/>
                                </Link>
                                {loggedIn && texts.length > 0?
                                    <Link to={`/foxy/texts/5b51e22a3bda3f04966b451f`} className='editAdminBtn'>
                                        <FontAwesomeIcon icon={faPencilAlt}/>
                                    </Link> : undefined
                                }
                            </li>
                            <li>
                                <img className='changeLang' src={lang == 'rus' ? ruIMG : enIMG} onClick={this.handleChangeLang}/>
                            </li>
                        </ul>
                    </div>
                </div>
                {editableText ? <Popup
                    modal
                    open={true}
                    closeOnDocumentClick
                >
                    <span> Modal content </span>
                </Popup> : undefined}

            </nav>
        )
    }
}

function mapStateToProps(reduxState) {
    return {
        texts: reduxState.texts,
        lang: reduxState.lang,
        activePage: reduxState.activePage,
        loggedIn: reduxState.loggedIn
    }
}

export default connect(mapStateToProps, {changeLang, changePage, getTexts})(Navbar);