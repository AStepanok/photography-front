import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom';

// Components
import Hero from './components/Pages/Hero';
import Navbar from "./components/UI/Navbar";
import NoMatch from './components/Pages/NoMatch';
import Login from './components/Pages/Login';
import ManipulateAlbum from "./components/Pages/Admin/ManipulateAlbum";
import ManipulateCategory from "./components/Pages/Admin/ManipulateCategory";
import ManipulateText from "./components/Pages/Admin/ManipulateText";
import Portfolio from './components/Pages/Portfolio';
import ViewAlbum from "./components/Pages/ViewAlbum";
import ManipulateVegasSlides from "./components/Pages/Admin/ManipulateVegasSlides";
import Prices from "./components/Pages/Prices";
import ManipulatePriceBox from "./components/Pages/Admin/ManipulatePriceBox";
import ManipulateReviewPhotos from "./components/Pages/Admin/ManipulateReviewPhotos";
import Reviews from "./components/Pages/Reviews";
import Contact from "./components/Pages/Contact";

// Libraries and CSS
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'
import './css/Admin.css'

library.add(faStroopwafel);

class App extends Component {

    render() {
        return (
          <div className="App">
              <Navbar/>

                <Switch>
                    <Route exact path='/' component={Hero}/>

                    <Route exact path='/foxy/categories' key='addCategory' component={ManipulateCategory}/>

                    <Route exact path='/foxy/categories/:id' key='editCategory' component={ManipulateCategory}/>

                    <Route exact path='/portfolio' component={Portfolio}/>

                    <Route exact path='/prices' component={Prices}/>

                    <Route exact path='/portfolio/:id' component={ViewAlbum}/>

                    <Route exact path='/reviews/' component={Reviews}/>

                    <Route exact path='/contacts/' component={Contact}/>

                    <Route exact path='/foxy' component={Login}/>

                    <Route exact path='/foxy/albums' key='addAlbum' component={ManipulateAlbum}/>

                    <Route exact path='/foxy/albums/:id' key='editAlbum' component={ManipulateAlbum}/>

                    <Route exact path='/foxy/priceboxes' key='addPriceBox' component={ManipulatePriceBox}/>

                    <Route exact path='/foxy/priceboxes/:id' key='editPriceBox' component={ManipulatePriceBox}/>

                    <Route exact path='/foxy/texts/:id' component={ManipulateText}/>

                    <Route exact path='/foxy/slides/' component={ManipulateVegasSlides}/>

                    <Route exact path='/foxy/reviews/' component={ManipulateReviewPhotos}/>

                    <Route component={NoMatch}/>
                </Switch>

          </div>
        );
    }
}

export default App;
