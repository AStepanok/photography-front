import {actions} from "./constants";

export function changePage(page) {
    return {
        type: actions.CHANGE_PAGE,
        page
    }
}