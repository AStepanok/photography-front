import {actions} from "./constants";

export function changeLang() {
    return {
        type: actions.LANG_CHANGE
    }
}