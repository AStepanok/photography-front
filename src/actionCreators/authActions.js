import {actions, API_URL} from "./constants";

export function login(data){
    return dispatch => {
        return fetch(`${API_URL}/users/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(data => data.json())
            .then(data => {
                localStorage.setItem('token', data.data.tokenID);
                dispatch(handleLogin());
            })
            .catch( (e) => console.log(e) );
    }
}

export function logout() {
    return dispatch => {
        localStorage.removeItem('token');
        dispatch(handleLogout());
    }
}

function handleLogin() {
    return {
        type: actions.LOGIN
    }
}

function handleLogout() {
    return {
        type: actions.LOGOUT
    }
}
