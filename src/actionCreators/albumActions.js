import {actions, API_URL} from "./constants";

export function getAlbums() {
    return dispatch => {
        return fetch(`${API_URL}/albums`)
            .then(data => data.json())
            .then(data => dispatch(handleGetAlbums(data)))
            .catch(err => console.log(err));
    }
}

export function addAlbum(album) {
    return dispatch => {
        return fetch(`${API_URL}/albums`, {
                method: 'post',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(album)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleAddAlbum(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function editAlbum(album) {
    return dispatch => {
        return fetch(`${API_URL}/albums/${album._id}`, {
                method: 'put',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(album)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditAlbum(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removeAlbum(album) {
    return dispatch => {
        return fetch(`${API_URL}/albums/${album._id}`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'delete'
            })
            .then(data => data.json())
            .then(data => dispatch(handleRemoveAlbum(album)))
            .catch(err => console.log(err));
    }
}

export function uploadPhotos(albumID, photos) {
    let data = new FormData();
    photos.forEach((photo) => {
        data.append('file', photo);
    });

    return dispatch => {
        return fetch(`${API_URL}/albums/${albumID}/photos`, {
            method: 'post',
            headers: new Headers({
                'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
            }),
            body: data
        })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditAlbum(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removePhoto(albumID, photoID) {
    return dispatch => {
        return fetch(`${API_URL}/albums/${albumID}/photos/${photoID}`, {
            headers: new Headers({
                'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
            }),
            method: 'delete'
        })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditAlbum(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

function handleGetAlbums(albums) {
    return {
        type: actions.GET_ALBUMS,
        albums
    }
}

function handleAddAlbum(album) {
    return {
        type: actions.ADD_ALBUM,
        album
    }
}

function handleEditAlbum(album) {
    return {
        type: actions.EDIT_ALBUM,
        album
    }
}

function handleRemoveAlbum(album) {
    return {
        type: actions.REMOVE_ALBUM,
        album
    }
}


