import {actions, API_URL} from './constants'

export function getReviewPhotos() {
    return dispatch => {
        return fetch(`${API_URL}/reviewphotos`)
            .then(data => data.json())
            .then(data =>
            {
                dispatch(handleGetReviewPhotos(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function uploadReviewPhotos(photos) {
    let data = new FormData();
    photos.forEach((photo) => {
        data.append('file', photo);
    });

    return dispatch => {
        return fetch(`${API_URL}/reviewphotos/`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'post',
                body: data
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditReviewPhotos(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removeReviewPhotos(photoID) {
    return dispatch => {
        return fetch(`${API_URL}/reviewphotos/${photoID}`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'delete',
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditReviewPhotos(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

function handleGetReviewPhotos(photos) {
    return {
        type: actions.GET_REVIEW_PHOTOS,
        photos
    }
}

function handleEditReviewPhotos(photos) {
    return {
        type: actions.EDIT_REVIEW_PHOTOS,
        photos
    }
}



