import {actions, API_URL} from "./constants";

export function getSlides() {
    return dispatch => {
        return fetch(`${API_URL}/vegasslides`)
            .then(data => data.json())
            .then(data => {
                dispatch(handleGetSlides(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function uploadSlides(slides) {
    let data = new FormData();
    slides.forEach((slide) => {
        data.append('file', slide);
    });

    return dispatch => {
        return fetch(`${API_URL}/vegasslides/`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'post',
                body: data
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditSlides(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removeSlide(slideID) {
    return dispatch => {
        return fetch(`${API_URL}/vegasslides/${slideID}`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'delete',
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditSlides(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

function handleGetSlides(slides) {
    return {
        type: actions.GET_SLIDES,
        slides
    }
}

function handleEditSlides(slides) {
    return {
        type: actions.EDIT_SLIDES,
        slides
    }
}
