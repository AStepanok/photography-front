import {actions, API_URL} from "./constants";

export function getCategories() {
    return dispatch => {
        return fetch(`${API_URL}/categories`)
            .then(data => data.json())
            .then(data => dispatch(handleGetCategories(data)))
            .catch(err => console.log(err));
    }
}

export function addCategory(cat) {
    return dispatch => {
        return fetch(`${API_URL}/categories`, {
                method: 'post',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(cat)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleAddCategory(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function editCategory(cat) {
    return dispatch => {
        return fetch(`${API_URL}/categories/${cat._id}`, {
                method: 'put',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(cat)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditCategory(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removeCategory(category) {
    return dispatch => {
        return fetch(`${API_URL}/categories/${category._id}`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'delete'
            })
            .then(data => data.json())
            .then(data => dispatch(handleRemoveCategory(category)))
            .catch(err => console.log(err));
    }
}

function handleGetCategories(cats) {
    return {
        type: actions.GET_CATEGORIES,
        cats
    }
}

function handleAddCategory(cat) {
    return {
        type: actions.ADD_CATEGORY,
        cat
    }
}

function handleEditCategory(cat) {
    return {
        type: actions.EDIT_CATEGORY,
        cat
    }
}

function handleRemoveCategory(category) {
    return {
        type: actions.REMOVE_CATEGORY,
        category
    }
}