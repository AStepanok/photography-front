import {actions, API_URL} from "./constants";

export function getTexts() {
    return dispatch => {
        return fetch(`${API_URL}/texts`)
            .then(data => data.json())
            .then(data => dispatch(handleGetTexts(data)))
            .catch(err => console.log(err));
    }
}

export function editText(text) {
    return dispatch => {
        return fetch(`${API_URL}/texts/${text._id}`, {
                method: 'put',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(text)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditText(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

function handleGetTexts(texts) {
    return {
        type: actions.GET_TEXTS,
        texts
    }
}

function handleEditText(text) {
    return {
        type: actions.EDIT_TEXT,
        text
    }
}