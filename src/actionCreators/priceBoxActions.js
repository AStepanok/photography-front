import {actions, API_URL} from "./constants";

export function getBoxes() {
    return dispatch => {
        return fetch(`${API_URL}/priceboxes`)
            .then(data => data.json())
            .then(data => dispatch(handleGetBoxes(data)))
            .catch(err => console.log(err));
    }
}

export function addBox(box) {
    return dispatch => {
        return fetch(`${API_URL}/priceboxes`, {
                method: 'post',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(box)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleAddBox(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function editBox(box) {
    return dispatch => {
        return fetch(`${API_URL}/priceboxes/${box._id}`, {
                method: 'put',
                headers: new Headers({
                    'Content-Type': 'application/json',
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                body: JSON.stringify(box)
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditBox(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function removeBox(boxId) {
    return dispatch => {
        return fetch(`${API_URL}/priceboxes/${boxId}`, {
                headers: new Headers({
                    'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
                }),
                method: 'delete',
            })
            .then(data => data.json())
            .then(data => {
                dispatch(handleRemoveBox(boxId));
                return data;
            })
            .catch(err => console.log(err));
    }
}

export function uploadBoxBack(boxId, photos) {
    let data = new FormData();
    photos.forEach((photo) => {
        data.append('file', photo);
    });

    return dispatch => {
        return fetch(`${API_URL}/priceboxes/${boxId}/photo`, {
            headers: new Headers({
                'Authorization' : `Bearer ${localStorage.getItem('token') || null}`
            }),
            method: 'post',
            body: data
        })
            .then(data => data.json())
            .then(data => {
                dispatch(handleEditBox(data));
                return data;
            })
            .catch(err => console.log(err));
    }
}

function handleGetBoxes(boxes) {
    return {
        type: actions.GET_BOXES,
        boxes
    }
}

function handleAddBox(box) {
    return {
        type: actions.ADD_BOX,
        box
    }
}

function handleEditBox(box) {
    return {
        type: actions.EDIT_BOX,
        box
    }
}

function handleRemoveBox(boxId) {
    return {
        type: actions.REMOVE_BOX,
        boxId
    }
}